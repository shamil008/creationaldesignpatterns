package AbstractFactory;

public interface Scout {
    void searchNewPlayer();
}

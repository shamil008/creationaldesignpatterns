package AbstractFactory;

public interface Player {
    void play();
}

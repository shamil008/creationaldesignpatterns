package AbstractFactory.BasketballTeam;

import AbstractFactory.Scout;

public class BasketballScout implements Scout {
    @Override
    public void searchNewPlayer() {
        System.out.println("Scout find new Basketball players");
    }
}

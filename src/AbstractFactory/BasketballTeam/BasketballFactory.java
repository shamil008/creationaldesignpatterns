package AbstractFactory.BasketballTeam;

import AbstractFactory.Coach;
import AbstractFactory.Player;
import AbstractFactory.Scout;
import AbstractFactory.TeamFactory;

public class BasketballFactory implements TeamFactory {
    @Override
    public Coach getCoach() {
        return new BasketballCoach();
    }

    @Override
    public Player getPlayer() {
        return new BasketballPlayer();
    }

    @Override
    public Scout getScout() {
        return new BasketballScout();
    }
}

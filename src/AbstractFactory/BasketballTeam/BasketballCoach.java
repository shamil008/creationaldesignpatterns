package AbstractFactory.BasketballTeam;

import AbstractFactory.Coach;

public class BasketballCoach implements Coach {
    @Override
    public void manageTeam() {
        System.out.println("Manage Basketball team");
    }
}

package AbstractFactory.BasketballTeam;

import AbstractFactory.Player;

public class BasketballPlayer implements Player {
    @Override
    public void play() {
        System.out.println("Play Basketball");
    }
}

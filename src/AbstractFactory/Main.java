package AbstractFactory;


import AbstractFactory.BasketballTeam.BasketballFactory;
import AbstractFactory.FootballTeam.FootballFactory;


public class Main {
    public static void main(String[] args) {
        var teamFactory = createPlayer(Sports.FOOTBALL);
        var coach = teamFactory.getCoach();
        var player = teamFactory.getPlayer();
        var scout = teamFactory.getScout();
        coach.manageTeam();
        player.play();
        scout.searchNewPlayer();

    }
    private static TeamFactory createPlayer(Sports sport){
        return switch (sport){
            case FOOTBALL -> new FootballFactory();
            case BASKETBALL -> new BasketballFactory();
        };

        }
}
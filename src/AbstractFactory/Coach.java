package AbstractFactory;

public interface Coach {
    void manageTeam();
}

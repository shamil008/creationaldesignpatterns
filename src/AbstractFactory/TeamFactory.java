package AbstractFactory;



public interface TeamFactory {
    Coach getCoach();
    Player getPlayer();
    Scout getScout();

}

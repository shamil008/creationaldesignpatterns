package AbstractFactory.FootballTeam;

import AbstractFactory.Scout;

public class FootballScout implements Scout {
    @Override
    public void searchNewPlayer() {
        System.out.println("Scout find new football players");
    }
}

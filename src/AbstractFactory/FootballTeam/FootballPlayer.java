package AbstractFactory.FootballTeam;

import AbstractFactory.Player;

public class FootballPlayer implements Player {
    @Override
    public void play() {
        System.out.println("Play Football");
    }
}

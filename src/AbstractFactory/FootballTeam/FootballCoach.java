package AbstractFactory.FootballTeam;

import AbstractFactory.Coach;

public class FootballCoach implements Coach {
    @Override
    public void manageTeam() {

        System.out.println("Coach Manage Football team");
    }
}

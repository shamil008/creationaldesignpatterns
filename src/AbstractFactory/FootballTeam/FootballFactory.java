package AbstractFactory.FootballTeam;

import AbstractFactory.Coach;
import AbstractFactory.Player;
import AbstractFactory.Scout;
import AbstractFactory.TeamFactory;

public class FootballFactory implements TeamFactory {
    @Override
    public Coach getCoach() {
        return new FootballCoach();
    }

    @Override
    public Player getPlayer() {
        return new FootballPlayer();
    }

    @Override
    public Scout getScout() {
        return new FootballScout();
    }
}

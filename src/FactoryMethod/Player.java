package FactoryMethod;

public interface Player {
    void play();
}

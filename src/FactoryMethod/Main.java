package FactoryMethod;

public class Main {
    public static void main(String[] args) {
        var playerFactory = createPlayer(Sports.FOOTBALL);
        var player = playerFactory.createPlayer();
        player.play();

    }
    private static PlayerFactory createPlayer(Sports sport){
        return switch (sport){
            case FOOTBALL -> new FootballPlayerFactory();
            case BASKETBALL -> new BasketballPlayerFactory();
        };

    }
}
package FactoryMethod;

public class FootballPlayer implements  Player{
    @Override
    public void play() {
        System.out.println("Player play Football");
    }
}

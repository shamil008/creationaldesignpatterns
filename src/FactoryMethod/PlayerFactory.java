package FactoryMethod;

public interface PlayerFactory {
    Player createPlayer();
}

package Singleton;

public class Main {
    public static void main(String[] args) {
        Singleton singleton = Singleton.INSTANCE;
        Singleton singleton1 = Singleton.INSTANCE;
        System.out.println(singleton1.hashCode() == singleton.hashCode());
        System.out.println(singleton1.hashCode());
        System.out.println(singleton.hashCode());
    }

}
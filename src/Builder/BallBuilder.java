package Builder;

public interface BallBuilder {
    void buildColour();
    void buildDesign();
    void buildSize();
    void buildMaterial();
    Ball getBall();
}

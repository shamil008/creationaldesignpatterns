package Builder;

public class Main {
    public static void main(String[] args) {
        BallBuilder football = new FootballBallBuilder();
        BallManager ballManager = new BallManager(football);
        ballManager.constructBall();
        Ball ball = football.getBall();
        System.out.println("Ball Colour : "+ball.getColour());
    }
}
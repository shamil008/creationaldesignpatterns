package Builder;

public class BallManager {
    private  BallBuilder ballBuilder;

    public BallManager(BallBuilder ballBuilder) {
        this.ballBuilder = ballBuilder;
    }
    public void constructBall(){
        ballBuilder.buildColour();
        ballBuilder.buildDesign();
        ballBuilder.buildSize();
        ballBuilder.buildMaterial();
    }
}

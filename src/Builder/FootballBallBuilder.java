package Builder;

public class FootballBallBuilder implements  BallBuilder{
    private  Ball ball;

    public FootballBallBuilder() {
        this.ball = new Ball();
    }

    @Override
    public void buildColour() {
        ball.setColour("Black & White");

    }

    @Override
    public void buildDesign() {
        ball.setDesign("Hexagonal");

    }

    @Override
    public void buildSize() {
        ball.setSize(64);

    }

    @Override
    public void buildMaterial() {
        ball.setMaterial("leather");

    }

    @Override
    public Ball getBall() {
        return this.ball;
    }
}

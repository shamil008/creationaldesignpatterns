package Prototype;

public interface Game {
    Object copy();
}

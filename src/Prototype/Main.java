package Prototype;

public class Main {
    public static void main(String[] args) {
        FS24 fs24original = new FS24(1,"2.5");
        FS24 fs24copy = (FS24) fs24original.copy();
        System.out.println("Original "+fs24original.toString());
        System.out.println("Copy "+fs24copy.toString());
    }
}
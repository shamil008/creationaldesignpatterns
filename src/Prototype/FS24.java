package Prototype;

public class FS24 implements Game {
    private  long id;
    private String version;

    public FS24(long id, String version) {
        this.id = id;
        this.version = version;
    }

    @Override
    public Object copy() {
        return new FS24(this.id,this.version);
    }

    @Override
    public String toString() {
        return "FS24{" +
                "id=" + id +
                ", version='" + version + '\'' +
                '}';
    }
}
